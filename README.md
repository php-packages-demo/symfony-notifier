# [symfony/notifier](https://packagist.org/packages/symfony/notifier)

Notify messages.

![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/notifier)

## Official documentation
* [*Creating and Sending Notifications*
  ](https://symfony.com/doc/current/notifier.html)
* [*Symfony Notifier*](https://speakerdeck.com/fabpot/symfony-notifier)
  2019-09 Fabien Potencier
